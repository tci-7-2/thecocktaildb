document.addEventListener("DOMContentLoaded", function() {
    document.getElementById('btnListar').addEventListener('click', function() {
        const cocktailType = document.querySelector('input[name="cocktail"]:checked').value;
        fetchCocktails(cocktailType);
    });

    document.getElementById('btnLimpiar').addEventListener('click', function() {
        clearCocktails();
    });

    function fetchCocktails(type) {
        fetch(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${type}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            displayCocktails(data.drinks, type);
        })
        .catch(error => {
            console.error('Error fetching cocktails:', error);
        });
    }

    function displayCocktails(cocktails, type) {
        const cocktailList = document.getElementById('cocktailList');
        const totalAlcoholic = document.getElementById('totalAlcoholic');
        const totalLabel = document.getElementById('totalLabel');

        cocktailList.innerHTML = '';
        let count = 0;

        if (type === 'alcoholic') {
            totalLabel.textContent = 'Total de cócteles con Alcohol:';
        } else {
            totalLabel.textContent = 'Total de cócteles sin Alcohol:';
        }

        if (cocktails) {
            cocktails.forEach(cocktail => {
                const cocktailItem = document.createElement('div');
                cocktailItem.classList.add('cocktail');

                const cocktailName = document.createElement('p');
                cocktailName.textContent = cocktail.strDrink;

                const cocktailImage = document.createElement('img');
                cocktailImage.src = cocktail.strDrinkThumb;
                cocktailImage.alt = cocktail.strDrink;
                cocktailImage.width = 100;
                cocktailImage.height = 100;

                cocktailItem.appendChild(cocktailName);
                cocktailItem.appendChild(cocktailImage);

                cocktailList.appendChild(cocktailItem);

                count++;
            });
        } else {
            cocktailList.textContent = 'No se encontraron cócteles';
        }

        totalAlcoholic.value = count;
    }

    function clearCocktails() {
        const cocktailList = document.getElementById('cocktailList');
        const totalAlcoholic = document.getElementById('totalAlcoholic');
        const totalLabel = document.getElementById('totalLabel');
    
        cocktailList.innerHTML = '';
        totalAlcoholic.value = '';
        
        // Verificar el tipo de cócteles mostrados y actualizar el texto del totalLabel en consecuencia
        const cocktailType = document.querySelector('input[name="cocktail"]:checked').value;
        if (cocktailType === 'alcoholic') {
            totalLabel.textContent = 'Total de cócteles con Alcohol:';
        } else {
            totalLabel.textContent = 'Total de cócteles sin Alcohol:';
        }
    }
});